# Test Chamber One

Welcome to the Vier für Texas web dev testing enrichment center.

Please take a brief look at the following tasks.

1) Use JavaScript to replace the H1-headline in index.html, you could use either the browser console or write your one-liner into the script tag.
2) Please explain why the script tag is not in the `<head>` section.
3) We would like to horizontally and vertically center the `<h1>` in the users viewport. Also we would like to have a red background on the `<h1>` element. Use the space within the `<style>` tag to add some CSS to this page.
4) Can you explain the the term `Document Object Model`?
5) Please open up `form.html` and `form.php`. 
   1) Can you submit form data to the processor and output the given string in ALL CAPS? 
   2) Can you reverse the string?
   3) What would you do to sanitize the processor?
   4) What security issues do you see?
6) Have some cake. 🎂